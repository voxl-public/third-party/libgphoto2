#!/bin/bash
################################################################################
# Copyright (c) 2020 ModalAI, Inc. All rights reserved.
################################################################################

cd libgphoto2-2.5.26
./configure --prefix=/home/root/pkg/data/usr
make -j8
cd -
